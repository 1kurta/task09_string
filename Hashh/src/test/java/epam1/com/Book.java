package epam1.com;

public class Book {
    private int id;
    private String title;


    public Book(int id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public String toString() {
        return id + ": " + title;
    }


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Book)) {
            return false;
        }
        return this.id == ((Book)o).id;
    }


    @Override
    public int hashCode() {
        return id;
    }
}