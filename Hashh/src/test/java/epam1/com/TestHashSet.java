package epam1.com;

import java.util.HashSet;
import java.util.Set;
public class TestHashSet {
    public static void main(String[] args) {
        Book book1 = new Book(1, "Java for Dummies");
        Book book1Dup = new Book(1, "Java for the Dummies");
        Book book2 = new Book(2, "Java for more Dummies");
        Book book3 = new Book(3, "more Java for more Dummies");

        Set<Book> set1 = new HashSet<Book>();
        set1.add(book1);
        set1.add(book1Dup);
        set1.add(book1);
        set1.add(book3);
        set1.add(null);
        set1.add(null);
        set1.add(book2);
        System.out.println(set1);

        set1.remove(book1);
        set1.remove(book3);
        System.out.println(set1);

        Set<Book> set2 = new HashSet<Book>();
        set2.add(book3);
        System.out.println(set2);
        set2.addAll(set1);
        System.out.println(set2);

        set2.remove(null);
        System.out.println(set2);
        set2.retainAll(set1);
        System.out.println(set2);
    }
}