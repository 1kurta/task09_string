package ira.com;

import java.util.Scanner;

public class Menu {

    public final static int IPhone 	= 1;
    public final static int Samsung = 2;
    public final static int Nokia 	= 3;
    public final static int Huawei	= 4;

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.println("Menu Phone");
        System.out.println("1 - IPhone");
        System.out.println("2 - Samsung");
        System.out.println("3 - Nokia");
        System.out.println("4 - Huawei");

        System.out.println("Digital: ");

        int opcao = entrada.nextInt();

        switch (opcao) {
            case IPhone:
                System.out.println("IPhone");
                break;
            case Samsung:
                System.out.println("Samsung");
                break;
            case Nokia:
                System.out.println("Nokia");
                break;
            case Huawei:
                System.out.println("Huawei");
                break;
            default:
                System.out.println("Opcao invalida");
                break;
        }
    }

}
