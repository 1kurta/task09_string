package hash.com;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Phone implements  View {

        private Map<String, String> phone;
        private Map<String, View> methodPhone;
        private static Scanner sc = new Scanner(System.in);
        private static Logger logger = LogManager.getLogger(Phone.class);

        private ResourceBundle bundle;
        private Locale locale;
        private Seller seller = new Seller();


        private void setPhone() {

            phone = new LinkedHashMap<>();
            phone.put("1", bundle.getString("1"));
            phone.put("2", bundle.getString("2"));
            phone.put("3", bundle.getString("3"));
            phone.put("4", bundle.getString("4"));
            phone.put("5", bundle.getString("5"));
            phone.put("6", bundle.getString("6"));
            phone.put("7", bundle.getString("7"));
            phone.put("Q", bundle.getString("8"));
        }

        public Phone() {

            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            methodPhone = new LinkedHashMap<>();
            methodPhone.put("1", this::IPhone 6);
            methodPhone.put("2", this::IPhone 7);
            methodPhone.put("3", this::IPhone 8+);
            methodPhone.put("4", this::IPhone X);
            methodPhone.put("5", this::IPhone 11);
            methodPhone.put("6", this::IPhone 7+);
            methodPhone.put("7", this::IPhone 11prom);

            showPhone();
            putAnswer();

        }



        public void setIPhone 6() {
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            showPhone();
        }

        public void setIPhone 7() {
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone ();
            showPhone ();
        }

        public void setIPhone 8+() {
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            showPhone();
        }

        public void setIPhone X() {
            locale = new Locale("el");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            showPhone();
        }

        public void setIPhone 11() {
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            showPhone();
        }


        public void setIPhone 7+() {
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            showPhone();
        }

        public void setIPhone 11prom() {
            locale = new Locale("en");
            bundle = ResourceBundle.getBundle("Phone", locale);
            setPhone();
            showPhone();
        }



        @Override
        public void showMenu() {
            for (String element : phone.values()) {
                System.out.println(element);
            }
        }

        public void putAnswer(){

            String keyPhone ="";
            do {

                System.out.println("Select appropriate option");
                keyPhone = sc.nextLine().toUpperCase();

                if(keyPhone.equals("1")||keyPhone.equals("2")) {
                    actionWithSeller(keyPhone;);
                }

                else  if(keyPhone.equals("Q")) {

                    System.exit(0);
                } else {
                    try {
                        methodPhone.get(keyPhone).showPhone();
                    } catch (Exception e) {
                    }
                }
            } while (keyPhone != "Q");
        }


        public void actionWithSeller(String key){

            switch(key){
                case "1" : seller.putValue(); break;
                case "2":
                    logger.info(seller.getValues());break;
            }

        }


    }

